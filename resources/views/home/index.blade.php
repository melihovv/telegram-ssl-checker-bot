@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <ul>
          <li><a href="{{ route('messages.create') }}">Send A Message To Subscribers</a></li>
          <li><a href="{{ route('subscribers.index') }}">Subscribers</a></li>
        </ul>
      </div>
    </div>
  </div>
@endsection
