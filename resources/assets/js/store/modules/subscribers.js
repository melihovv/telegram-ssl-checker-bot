import Vue from 'vue'
import {
  CLEAR_ERROR, CLEAR_SELECTED, SET_CURRENT_PAGE, SET_ERROR, SET_PAGINATION_DATA, SET_SUBSCRIBERS, SET_TOTAL,
  TOGGLE_SUBSCRIBER,
} from '../mutation-types'

export default {
  namespaced: true,
  state: {
    subscribers: [],
    paginationData: {},
    total: 0,
    selected: {},
    currentPage: 1,
    error: '',
  },
  mutations: {
    [SET_SUBSCRIBERS](state, subscribers) {
      state.subscribers = subscribers
    },
    [CLEAR_ERROR](state) {
      state.error = ''
    },
    [SET_ERROR](state, error) {
      state.error = error
    },
    [SET_CURRENT_PAGE](state, page) {
      state.currentPage = page
    },
    [SET_TOTAL](state, total) {
      state.total = total
    },
    [SET_PAGINATION_DATA](state, paginationData) {
      state.paginationData = paginationData
    },
    [TOGGLE_SUBSCRIBER](state, telegramId) {
      if (state.selected[telegramId]) {
        Vue.delete(state.selected, telegramId)
      } else {
        Vue.set(state.selected, telegramId, 1)
      }
    },
    [CLEAR_SELECTED](state) {
      state.selected = {}
    },
  },
  getters: {
    selectedIds: state => {
      return Object.keys(state.selected)
    },
    selectedAmount: (state, getters) => {
      return getters.selectedIds.length
    },
    noOneIsSelected: (state, getters) => {
      return getters.selectedAmount === 0
    },
  }
}
