<?php

namespace App\Http\Requests;

class SubscribersBatchDeleteRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subscribers_ids' => 'required|array',
            'subscribers_ids.*' => 'exists:subscribers,telegram_id',
        ];
    }
}
