<?php

namespace App\Http\Requests;

class MessageStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required|string|min:1',
            'subscribers_ids' => 'nullable|array',
            'subscribers_ids.*' => 'exists:subscribers,telegram_id',
        ];
    }
}
