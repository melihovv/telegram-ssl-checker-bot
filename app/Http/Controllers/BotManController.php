<?php

namespace App\Http\Controllers;

use App\Models\Subscriber;
use App\Ssl\SslChecker;
use BotMan\BotMan\BotMan;
use Throwable;

class BotManController extends Controller
{
    public function handle()
    {
        app('botman')->listen();
    }

    public function sslInfo(BotMan $bot, $host)
    {
        try {
            $certificate = app(SslChecker::class)->check($host);
        } catch (Throwable $e) {
            $bot->reply('Error! Check domain again.');

            return;
        }

        $bot->reply('Issuer: ' . $certificate->getIssuer());
        $bot->reply('Is Valid: ' . ($certificate->isValid() ? 'True' : 'False'));
        $bot->reply('Expired In: ' . $certificate->expirationDate()->diffInDays() . ' days');
    }

    public function subscribe(BotMan $bot)
    {
        $user = $bot->getUser();
        $userId = $user->getId();

        Subscriber::firstOrCreate(['telegram_id' => $userId], [
            'telegram_id' => $userId,
            'telegram_username' => $user->getUsername(),
        ]);

        $bot->reply('You was successfully subscribed!');
    }

    public function unsubscribe(BotMan $bot)
    {
        Subscriber::where('telegram_id', $bot->getUser()->getId())->delete();

        $bot->reply('You was successfully unsubscribed!');
    }

    public function fallback(BotMan $bot)
    {
        $bot->reply('Sorry, I did not understand these commands.');
    }
}
