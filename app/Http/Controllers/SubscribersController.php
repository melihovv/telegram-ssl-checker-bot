<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubscribersBatchDeleteRequest;
use App\Models\Subscriber;

class SubscribersController extends Controller
{
    public function index()
    {
        if (request()->wantsJson()) {
            return Subscriber::paginate(5);
        }

        return view('subscribers.index');
    }

    public function batchDelete(SubscribersBatchDeleteRequest $request)
    {
        Subscriber::whereIn('telegram_id', $request->subscribers_ids)->delete();

        return response()->json();
    }
}
