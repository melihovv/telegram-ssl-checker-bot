<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageStoreRequest;
use App\Models\Subscriber;
use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\TelegramDriver;
use Throwable;

class MessagesController extends Controller
{
    public function create()
    {
        return view('messages.create');
    }

    public function store(MessageStoreRequest $request)
    {
        try {
            $subscribersIds = $request->subscribers_ids;

            if (empty($subscribersIds)) {
                $subscribersIds = Subscriber::pluck('telegram_id');
            }

            // TODO how to send message to multiple users at once? https://github.com/botman/botman/issues/660
            foreach ($subscribersIds as $subscriberId) {
                Botman::say($request->message, $subscriberId, TelegramDriver::class);
            }
        } catch (Throwable $e) {
            report($e);

            return response()->json(['error' => $e->getMessage()], 500);
        }

        return response()->json();
    }
}
