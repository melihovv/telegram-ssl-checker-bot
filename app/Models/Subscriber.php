<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    use AdditionalMethods;

    protected $fillable = [
        'telegram_id',
        'telegram_username',
    ];

    protected $casts = [
        'telegram_id' => 'integer',
    ];
}
