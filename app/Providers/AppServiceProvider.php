<?php

namespace App\Providers;

use App\Ssl\SpatieSslChecker;
use App\Ssl\SslChecker;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SslChecker::class, SpatieSslChecker::class);
    }
}
