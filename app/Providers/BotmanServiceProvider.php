<?php

namespace App\Providers;

use BotMan\BotMan\Drivers\DriverManager;
use BotMan\Drivers\Telegram\TelegramDriver;

class BotmanServiceProvider extends \BotMan\BotMan\BotManServiceProvider
{
    public function boot()
    {
        DriverManager::loadDriver(TelegramDriver::class);

        require base_path('routes/botman.php');
    }
}
