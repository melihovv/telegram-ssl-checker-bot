<?php

declare(strict_types = 1);

namespace App\Ssl;

use Spatie\SslCertificate\SslCertificate;

interface SslChecker
{
    /**
     * Check ssl certificate of specified domain.
     *
     * @param string $domain
     * @param int $timeout In seconds
     * @return SslCertificate
     */
    public function check($domain, $timeout = 30);
}
