<?php

declare(strict_types = 1);

namespace App\Ssl;

use Spatie\SslCertificate\SslCertificate;

class FakeSslChecker implements SslChecker
{
    /**
     * @inheritdoc
     */
    public function check($domain, $timeout = 30)
    {
        return new SslCertificate([
            'issuer' => ['CN' => 'Let\'s Encrypt Authority X3'],
            'validFrom_time_t' => now()->subMonth()->timestamp,
            'validTo_time_t' => now()->addWeek()->timestamp,
        ]);
    }
}
