<?php

declare(strict_types = 1);

namespace App\Ssl;

use Spatie\SslCertificate\SslCertificate;

class SpatieSslChecker implements SslChecker
{
    /**
     * @inheritdoc
     */
    public function check($domain, $timeout = 30)
    {
        return SslCertificate::createForHostName($domain);
    }
}
