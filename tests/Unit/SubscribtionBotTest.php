<?php

namespace Tests\Unit;

use App\Models\Subscriber;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SubscribtionBotTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function user_can_subscribe()
    {
        $this->bot
            ->setUser(['id' => 12345])
            ->receives('subscribe')
            ->assertReply('You was successfully subscribed!');

        $this->assertDatabaseHas('subscribers', [
            'telegram_id' => 12345,
        ]);
    }

    /** @test */
    function subscribed_user_can_subscribe()
    {
        $subscriber = factory(Subscriber::class)->create();

        $this->bot
            ->setUser(['id' => $subscriber->telegram_id])
            ->receives('subscribe')
            ->assertReply('You was successfully subscribed!');

        $this->assertDatabaseHas('subscribers', [
            'telegram_id' => $subscriber->telegram_id,
        ]);
        $this->assertEquals(1, Subscriber::count());
    }

    /** @test */
    function subscribed_user_can_unsubscribe()
    {
        $subscriber = factory(Subscriber::class)->create();

        $this->bot
            ->setUser(['id' => $subscriber->telegram_id])
            ->receives('unsubscribe')
            ->assertReply('You was successfully unsubscribed!');

        $this->assertEquals(0, Subscriber::count());
    }

    /** @test */
    function unsubscribed_user_can_unsubscribe()
    {
        $this->bot
            ->setUser(['id' => 12345])
            ->receives('unsubscribe')
            ->assertReply('You was successfully unsubscribed!');

        $this->assertEquals(0, Subscriber::count());
    }
}
