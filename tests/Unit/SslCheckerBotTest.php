<?php

namespace Tests\Unit;

use App\Ssl\FakeSslChecker;
use App\Ssl\SpatieSslChecker;
use App\Ssl\SslChecker;
use Spatie\SslCertificate\Exceptions\CouldNotDownloadCertificate;
use Spatie\SslCertificate\Exceptions\InvalidUrl;
use Tests\TestCase;

class SslCheckerBotTest extends TestCase
{
    /** @test */
    function it_responds_to_user_with_site_ssl_certificate_info()
    {
        $this->app->instance(SslChecker::class, new FakeSslChecker);

        $this->bot
            ->receives('ssl-info b2beer.ru')
            ->assertReplies([
                'Issuer: Let\'s Encrypt Authority X3',
                'Is Valid: True',
                'Expired In: 7 days',
            ]);
    }

    /** @test */
    function it_responds_to_user_with_error_if_it_cannot_retrieve_ssl_info()
    {
        $this->app->instance(SslChecker::class, $mock = $this->mockInstance(SpatieSslChecker::class));
        $mock->shouldReceive('check')->with('gv4.ru')->andThrow(CouldNotDownloadCertificate::class);

        $this->bot
            ->receives('ssl-info gv4.ru')
            ->assertReply('Error! Check domain again.');
    }

    /** @test */
    function it_responds_to_user_with_error_if_he_did_specified_not_valid_domain()
    {
        $this->app->instance(SslChecker::class, $mock = $this->mockInstance(SpatieSslChecker::class));
        $mock->shouldReceive('check')->with('abc')->andThrow(InvalidUrl::class);

        $this->bot
            ->receives('ssl-info abc')
            ->assertReply('Error! Check domain again.');
    }
}
