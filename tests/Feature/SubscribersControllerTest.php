<?php

declare(strict_types = 1);

namespace Tests\Feature;

use App\Models\Subscriber;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SubscribersControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_show_subscribers_page()
    {
        $this
            ->get(route('subscribers.index'))
            ->assertStatus(200)
            ->assertViewIs('subscribers.index');
    }

    /** @test */
    function it_returns_subscribers()
    {
        $subscribers = factory(Subscriber::class, 10)->create();

        $this
            ->getJson(route('subscribers.index'))
            ->assertStatus(200)
            ->assertJsonFragment([
                'telegram_id' => $subscribers->first()->telegram_id,
                'telegram_username' => $subscribers->first()->telegram_username,
            ]);
    }

    /** @test */
    function it_batch_deletes_subscribers()
    {
        $subscribers = factory(Subscriber::class, 10)->create();

        $this
            ->deleteJson(route('subscribers.batch-delete'), [
                'subscribers_ids' => $subscribers->take(5)->pluck('telegram_id')->toArray(),
            ])
            ->assertStatus(200);

        foreach ($subscribers->take(5) as $subscriber) {
            $this->assertDatabaseMissing('subscribers', [
                'telegram_id' => $subscriber->telegram_id,
            ]);
        }
        foreach ($subscribers->splice(5) as $subscriber) {
            $this->assertDatabaseHas('subscribers', [
                'telegram_id' => $subscriber->telegram_id,
            ]);
        }
    }

    /** @test */
    function it_returns_422_if_validation_does_not_pass_during_batch_delete()
    {
        $this
            ->deleteJson(route('subscribers.batch-delete'), [
                'subscribers_ids' => [100500],
            ])
            ->assertStatus(422);
    }
}
