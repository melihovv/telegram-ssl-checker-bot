<?php

declare(strict_types = 1);

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HomeControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_shows_home_page()
    {
        $this
            ->get(route('home.index'))
            ->assertStatus(200)
            ->assertViewIs('home.index');
    }
}
