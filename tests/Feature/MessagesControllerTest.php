<?php

declare(strict_types = 1);

namespace Tests\Feature;

use App\Models\Subscriber;
use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MessagesControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_shows_create_page()
    {
        $this
            ->get(route('messages.create'))
            ->assertStatus(200)
            ->assertViewIs('messages.create');
    }

    /** @test */
    function it_sends_message_to_subscribers()
    {
        $subscriber = factory(Subscriber::class)->create();
        Botman::shouldReceive('say')->with('Hello', $subscriber->telegram_id, TelegramDriver::class);

        $this
            ->postJson(route('messages.store'), ['message' => 'Hello'])
            ->assertStatus(200);
    }

    /** @test */
    function it_sends_message_to_specified_subscribers()
    {
        $subscribers = factory(Subscriber::class, 2)->create();
        Botman::shouldReceive('say')->with('Hello', $subscribers[1]->telegram_id, TelegramDriver::class);

        $this
            ->postJson(route('messages.store'), [
                'message' => 'Hello',
                'subscribers_ids' => [$subscribers[1]->telegram_id],
            ])
            ->assertStatus(200);
    }

    /** @test */
    function it_returns_422_if_validation_does_not_pass_during_batch_delete()
    {
        $this
            ->postJson(route('messages.store'), [
                'message' => '',
                'subscribers_ids' => [100500],
            ])
            ->assertStatus(422);
    }
}
