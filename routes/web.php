<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::match(['get', 'post'], '/botman', 'BotManController@handle');

Route::resource('messages', 'MessagesController')->only(['create', 'store']);

Route::delete('subscribers/batch-delete', 'SubscribersController@batchDelete')->name('subscribers.batch-delete');

Route::resource('subscribers', 'SubscribersController')->only(['index']);

Route::get('/', 'HomeController@index')->name('home.index');
