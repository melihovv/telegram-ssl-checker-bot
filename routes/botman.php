<?php

use App\Http\Controllers\BotManController;

/** BotMan $botman */

$botman = resolve('botman');

$botman->hears('ssl-info {host}', BotManController::class . '@sslInfo');
$botman->hears('subscribe', BotManController::class . '@subscribe');
$botman->hears('unsubscribe', BotManController::class . '@unsubscribe');

$botman->fallback(BotManController::class . '@fallback');
