<?php

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/**
 * @var Factory $factory
 */
$factory->define(App\Models\Subscriber::class, function (Faker $faker) {
    return [
        'telegram_id' => $faker->unique()->randomNumber(),
        'telegram_username' => $faker->userName,
    ];
});
