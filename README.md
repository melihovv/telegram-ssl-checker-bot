# Задание
 
1 При помощи Laravel и пакета https://botman.io сделать телеграм-бота, который бы проверял SSL-сертификат домена по команде. Пример https://t.me/RhinodontypicusBot , вводим `ssl-info {domain}` (например `ssl-info intraweb.agency`) , получаем информацию. Если домен недоступен то выводим ошибку http://take.ms/0fDTq . Код заливаем на github/bitbucket.

Важно: не использовать BotMan Studio - botman нужно подключить пакетом к чистому Laravel.

2 Пользователь может написать боту команду subscribe — подписка на сообщения от админа. Сообщения пишутся во Vue форме на сайте (там же где бот лежит), без аутентификации и т.д, просто Vue форма с полем сообщения. Заполняем поле, отправляем — всем пользователям которые написали в telegram subscribe отправляется это сообщение. Команда unsubscribe должна отписывать пользователя от сообщений.

3 Вывести: http://take.ms/sgOBQ . Пагинация должна быть клиентская, выбранные элементы не должны сбрасываться при переходе между страницами. При удалении спрашивать подтверждение. При нажатии send message to selected ниже появляется поле для ввода сообщения и send кнопка. Желательно использовать Vuex. 
    
Желательно разместить все это на своем каком-то сервере под своим доменом. Если его нет, то сообщите дополнительно, создадим у себя на субдомене на тестовом сервере.
 
Обязательно засеките время выполнения задачи.

# Requirements
- git
- php>=7.0
- composer
- ngrok (for development only)

# Install
- `git clone https://bitbucket.org/melihovv/telegram-ssl-checker-bot`
- `cd telegram-ssl-checker-bot`
- `composer install`
- `cp .env.example .env`
- `php artisan key:generate`
- specify DB_* variables in .env file
- create telegram bot with the help of BotFather
    - `/newbot name-of-your-bot` and then follow the instructions
    - specify given token in .env file in TELEGRAM_TOKEN variable
- php artisan serve
- ngrok http 8000
- `curl --data "url=https://<see previous command output>.ngrok.io/botman" https://api.telegram.org/bot<your-telegram-token>/setWebhook`  
  or (with httpie)  
  `http https://api.telegram.org/bot<your-telegram-token>/setWebhook url=https://<see previous command output>.ngrok.io/botman`

# Available commands
- `ssl-info some-domain.com` Replies with the main info about ssl certificate of specified domain.
- `subscribe` Subscribes user to admin messages.
- `unsubscribe` Unsubscribe user from admin messages.

# Admin UI
- `/messages/create` Form to send message to subscribed users.
- `/subscribers` Page to see list of subscribed users with ability to delete selected users or send message to them.
